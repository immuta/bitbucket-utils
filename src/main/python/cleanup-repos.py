#!/usr/bin/env python

import argparse
import vault

from bitbucket.bitbucket import BitBucket
from github import Github # pip install PyGithub

def fetch_repo_names_from_github(github_org):
    github_ref = Github()
    gh_org = github_ref.get_organization(github_org)
    ret_val = []    
    for repo in gh_org.get_repos():
        ret_val.append(repo.name)
    return ret_val

def setup_keyring(val, service, key):
    if not val:
        return vault.get(service, key)
    else:
        # reset our keyring just in case
        vault.set(service, key, val)
        return val

def main():
    script_desc = "Script to clean your bitbucket account of certain repos"
    arg_parser = argparse.ArgumentParser(script_desc)
    arg_parser.add_argument("--repos-to-delete", "-r", default=None,
                           metavar="CSV_OF_REPOS_TO_DELETE", 
                           help="A CSV of repos to delete.")
    arg_parser.add_argument("--github-organization", "-g",
                           metavar="GITHUB_ORGANIZATION", default=None, 
                           help=("Use the public repos from a github" 
                                 " organization"))
    arg_parser.add_argument("--bitbucket-username", 
                           metavar="BITBUCKET_USERNAME",
                           help=("Bitbucket username if nothing is passed then"
                                " it is obtained through the keyring or"
                                " prompting."))
    arg_parser.add_argument("--bitbucket-password", 
                           metavar="BITBUCKET_PASSWORD",
                           help=("Bitbucket password if nothing is passed then" 
                                 " it is obtained through the keyring or"
                                 " prompting."))
    arg_parser.add_argument("--bitbucket-repo-owner",
                            metavar="BITBUCKET_REPO_OWNER", required=True,
                            help=("The owner of the bitbucket repositories to" 
                            " clean"))
    args = arg_parser.parse_args()

    repos_to_delete = None
    if args.repos_to_delete:
        repos_to_delete = args.repos_to_delete.split(",")
    elif args.github_organization:
        repos_to_delete = fetch_repo_names_from_github(args.github_organization)
    
    bb_username = setup_keyring(args.bitbucket_username, "bitbucket.org",
                               "username")
    bb_password = setup_keyring(args.bitbucket_password, "bitbucket.org",
                               "password")

    bb = BitBucket(bb_username, bb_password)
    repo_owner = args.bitbucket_repo_owner
    for repo_to_delete in repos_to_delete:
        repo_to_delete = repo_to_delete.lower()
        if not bb.delete_repo(repo_owner, repo_to_delete):
            print "unable to delete: %s\n" % repo_to_delete
            continue
    
    print "Have a nice day!"

if __name__ == '__main__':
    main()
