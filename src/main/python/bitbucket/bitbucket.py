import json

from requests import Request, Session # pip install requests

BITBUCKET_BASE_URL = "https://api.bitbucket.org/2.0"
DEFAULT_REPO_OPTIONS = {u"scm" : u"git", u"is_private" : u"true", u"fork_policy" : "no_public_forks"}

class BitBucket(object):
    def __init__(self, username="", password=""):
        self.username = username
        self.password = password

    @property
    def auth_string(self):
        return (self.username, self.password)

    def create_repo(self, repo_owner, repo_name):
        repo_url = "%s/repositories/%s/%s" % (BITBUCKET_BASE_URL, repo_owner,
                                              repo_name)
        return self._dispatch("POST", repo_url, DEFAULT_REPO_OPTIONS)

    def delete_repo(self, repo_owner, repo_name):
        repo_url = "%s/repositories/%s/%s" % (BITBUCKET_BASE_URL, repo_owner,
                                              repo_name)
        print repo_url
        return self._dispatch("DELETE", repo_url)
        

    def _dispatch(self, request_method, repo_url, request_data=None):
        request = None
        if request_data:
            request = Request(request_method, url = repo_url, 
                              auth = self.auth_string, data = request_data)
        else:
            request = Request(request_method, url = repo_url, 
                              auth = self.auth_string)

        session = Session()
        response = session.send(request.prepare())
        status = response.status_code
        reason = response.reason
        text = response.text
        if status >= 200 and status < 300:
            if text:
                print "Response Text: %s\n" % text        
            return True
        elif status >= 300 and status < 400:
            self._error_msg(
                "Unauthorized Access, please check your credentials!", text)
            return False
        elif status >= 400:
            self._error_msg(
                "Response Status %d" % status, text)
            return False

    def _error_msg(self, error_msg, response_text):
        if response_text:
            error_msg += "Response Text: %s\n" % response_text
        print error_msg
