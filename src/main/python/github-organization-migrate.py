#!/usr/bin/env python

import argparse
import glob
import os
import shutil
import subprocess
import sys
import vault # pip install vault

from github import Github # pip install PyGithub
from bitbucket.bitbucket import BitBucket

def clone_from_github(clone_dir, github_org):
    # cd ${clone_dir}
    os.chdir(clone_dir)

    # Fetch Repos from github
    github_ref = Github()
    gh_org = github_ref.get_organization(github_org)
    for repo in gh_org.get_repos():
        subprocess.check_call(['git', 'clone', repo.clone_url, repo.name])


def update_remote_and_push_to_bitbucket(clone_dir, bb_username, bb_password, bb_repo_owner):
    os.chdir(clone_dir)
    
    b = BitBucket(bb_username, bb_password)

    # Loop through our directory
    for name in glob.iglob('*'):
        # Bitbucket requires all lower case names for repositories
        name = name.lower()

        # Create the repo in bitbucket
        if not b.create_repo(bb_repo_owner, name):
            # Skip if we had an error and continue
            # TODO come up with a better way to handle errors
            print "Skipping: %s " % name
            continue

        # cd to the repo we are working on
        os.chdir(name)

        # Update our remote repository
        push_url = 'git@bitbucket.org:%s/%s.git' % (bb_repo_owner, name)

        # TODO Come up with a cleaner to move all branches over
        subprocess.check_call(['git', 'remote', 'set-url', 'origin', push_url])
        subprocess.check_call(['git', 'push', 'origin', 'master'])

        # cd to the base directory
        os.chdir(clone_dir)
        

def main():

    script_desc = ("This script will migrate all public github repositories of"
                   " a given user or organization to a bitbucket user or team.")

    parser = argparse.ArgumentParser(description=script_desc)

    parser.add_argument("--clone-directory", "-c", required=True,
                        metavar="LOCAL_DIRECTORY_TO_CLONE_TO", 
                        help="The local directory to clone to.")

    parser.add_argument("--create-clone-dir", "-x", action="store_true", 
                        help=("Create clone directory and intermediate"
                              " directories if they don't exist"))

    parser.add_argument("--preserve-clone-directory", "-p", action="store_true",
                        help="Do NOT clean up clone directory after we're done")

    parser.add_argument("--bitbucket-username", default=None,
                        metavar="USERNAME", 
                        help=("BitBucket username if nothing is passed then it"
                              " is obtained through the keyring or by prompting"
                              " the user."))

    parser.add_argument("--bitbucket-password", default=None,
                        metavar="PASSWORD", help=("BitBucket password if "
                                                  " nothing is passed then it"
                                                  " is obtained through the"
                                                  " keyring or by prompting the"
                                                  " user."))

    parser.add_argument("--bitbucket-repo-owner", required=True,
                        metavar="OWNER", help=("The owner of the bitbucket" 
                                               " repository"))

    parser.add_argument("--github-organization", "-o", required=True,
                        metavar="GITHUB_ORGANIZATION", help=("The name of the"
                                                              " github"
                                                              " organization!"))

    args = parser.parse_args()

    clone_dir = args.clone_directory
    gh_org = args.github_organization
    if not os.path.exists(clone_dir):
        if args.create_clone_dir:
            os.makedirs(clone_dir)
        else:
            error_msg =("Clone directory %s does not exist, try passing"
                       " --create-clone-dir to create it") % clone_dir
            raise RuntimeError(error_msg)

    clone_dir = os.path.join(clone_dir, gh_org)
    if not os.path.exists(clone_dir):
        os.mkdir(clone_dir)

    bb_username = args.bitbucket_username
    if not bb_username:
        bb_username = vault.get("bitbucket.org", "username")
    else:
        vault.set("bitbucket.org", "username", bb_username)
    
    bb_password = args.bitbucket_password
    if not bb_password:
        bb_password = vault.get("bitbucket.org", "password")
    else:
        vault.set("bitbucket.org", "password", bb_password)

    clone_from_github(clone_dir, gh_org)
    update_remote_and_push_to_bitbucket(clone_dir, bb_username, bb_password, 
                                        args.bitbucket_repo_owner)
    
    if args.preserve_clone_directory:
        sys.exit(0)
    else:
        shutil.rmtree(clone_dir)
    sys.exit(0)
        

if __name__ == '__main__':
    main()
