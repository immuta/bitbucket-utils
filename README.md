## Synopsis
This project provides various tools to use with bitbucket. 

### github-organization-migrate.py
A python script which will take a organization in github and will migrate its
public repositories from github to bitbucket.  It will mark the new repository
as private.

#### usage
```bash
$ python src/main/python/github-organization-migrate.py --help
usage: github-organization-migrate.py [-h] --clone-directory
                                      LOCAL_DIRECTORY_TO_CLONE_TO
                                      [--create-clone-dir]
                                      [--preserve-clone-directory]
                                      [--bitbucket-username USERNAME]
                                      [--bitbucket-password PASSWORD]
                                      --bitbucket-repo-owner OWNER
                                      --github-organization
                                      GITHUB_ORGANIZATION
This script will migrate all public github repositories of a given user or 
organization to a bitbucket user or team.

optional arguments:
   -h, --help            show this help message and exit
   --clone-directory LOCAL_DIRECTORY_TO_CLONE_TO, -c LOCAL_DIRECTORY_TO_CLONE_TO
                         The local directory to clone to.
   --create-clone-dir, -x
                         Create clone directory and intermediate directories if
                         they don't exist
   --preserve-clone-directory, -p
                         Do NOT clean up clone directory after we're done
  --bitbucket-username USERNAME
                         BitBucket username if nothing is passed then it is
                         obtained through the keyring or by prompting the user.
  --bitbucket-password PASSWORD
                         BitBucket password if nothing is passed then it is
                         obtained through the keyring or by prompting the user.
  --bitbucket-repo-owner OWNER
                         The owner of the bitbucket repository
  --github-organization GITHUB_ORGANIZATION, -o GITHUB_ORGANIZATION
                         The name of the github organization!
```
##### Sample Usage
```bash
$ python src/main/python/github-organization-migrate.py --clone-directory /tmp/ --create-clone-dir --bitbucketuser-username test --bitbucket-repo-owner test-team --github-organization facebook
```

#### Dependencies
   + python 2.7 or higher
   + git binaries installed on the machine and accessible by the user running the script
   + pip install PyGithub
   + pip install requests
   + pip install vault

#### Installation
```bash
git clone https://<bitbucket username>@bitbucket.org/immuta/bitbucket-utils.git
```

#### License
This software is licensed under the Apache 2 license, quoted below.

    Copyright (c) 2013 Elasticsearch <http://www.elasticsearch.org>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0
   
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

### cleanup-repos.py
Remove a list of repositories from bitbucket.  You can give it a CSV or a github organization where it will get the public repsoitory names to delete form.

#### Usage
```bash
python src/main/python/cleanup-repos.py --help
usage: Script to clean your bitbucket account of certain repos
       [-h] [--repos-to-delete CSV_OF_REPOS_TO_DELETE]
       [--github-organization GITHUB_ORGANIZATION]
       [--bitbucket-username BITBUCKET_USERNAME]
       [--bitbucket-password BITBUCKET_PASSWORD] 
       --bitbucket-repo-owner
       BITBUCKET_REPO_OWNER
 
optional arguments:
   -h, --help            show this help message and exit
  --repos-to-delete CSV_OF_REPOS_TO_DELETE, -r CSV_OF_REPOS_TO_DELETE
                         A CSV of repos to delete.
  --github-organization GITHUB_ORGANIZATION, -g GITHUB_ORGANIZATION
                         Use the public repos from a github organization
  --bitbucket-username BITBUCKET_USERNAME
                         Bitbucket username if nothing is passed then it is
                         obtained through the keyring or prompting.
  --bitbucket-password BITBUCKET_PASSWORD
                         Bitbucket password if nothing is passed then it is
                         obtained through the keyring or prompting.
  --bitbucket-repo-owner BITBUCKET_REPO_OWNER
                         The owner of the bitbucket repositories to clean
```

##### Sample Usage
```bash
$ python src/main/python/cleanup-repos.py --github-organization facebook --bitbucket-repo-owner immuta
```

#### Dependencies
   + python 2.7 or higher
   + pip install PyGithub
   + pip install requests
   + pip install vault

#### Installation
```bash
git clone https://<bitbucket username>@bitbucket.org/immuta/bitbucket-utils.git
```

#### License
This software is licensed under the Apache 2 license, quoted below.

    Copyright (c) 2013 Elasticsearch <http://www.elasticsearch.org>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0
   
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

